# Installing Jenkins on a Server 2024

### Pre-Requisites
Digital Ocean Droplet

Docker

Jenkins

#### Project Outline

In order for our future piplelines to build and test Java and JavaScript script applications we will need to execute them using their respecitve commands, in order to do that wen need to make Maven and npm available in Jenkins.


#### Maven Plugin Installation

Lets navigate to Jenkins and then navigate to Tools and then Maven Installations, and save the latest versions from Apache. This will install Maven plugin.

![Image 1](https://gitlab.com/FM1995/install-build-tools-in-jenkins-2024/-/raw/main/Images/Image1.png?ref_type=heads)
![Image 2](https://gitlab.com/FM1995/install-build-tools-in-jenkins-2024/-/raw/main/Images/Image2.png?ref_type=heads)
![Image 3](https://gitlab.com/FM1995/install-build-tools-in-jenkins-2024/-/raw/main/Images/Image3.png?ref_type=heads)
![Image 4](https://gitlab.com/FM1995/install-build-tools-in-jenkins-2024/-/raw/main/Images/Image4.png?ref_type=heads)
![Image 5](https://gitlab.com/FM1995/install-build-tools-in-jenkins-2024/-/raw/main/Images/Image5.png?ref_type=heads)


#### NodeJS npm Installation

We will now install nodejs and npm

Check the Jenkins docker container using Docker ps

![Image 6](https://gitlab.com/FM1995/install-build-tools-in-jenkins-2024/-/raw/main/Images/Image6.png?ref_type=heads)

Go into the container as a root user

```
docker exec -u 0 -it 659cfc6fbc3f bash
```
![Image 7](https://gitlab.com/FM1995/install-build-tools-in-jenkins-2024/-/raw/main/Images/Image7.png?ref_type=heads)

Ensure all updates are in place

![Image 8](https://gitlab.com/FM1995/install-build-tools-in-jenkins-2024/-/raw/main/Images/Image8.png?ref_type=heads)

Install curl

```
apt install curl
```

![Image 9](https://gitlab.com/FM1995/install-build-tools-in-jenkins-2024/-/raw/main/Images/Image9.png?ref_type=heads)

Can download the nodesource_setup.sh script

```
curl -sL https://deb.nodesource.com/setup_10.x -o nodesource_setup.sh
```

![Image 10](https://gitlab.com/FM1995/install-build-tools-in-jenkins-2024/-/raw/main/Images/Image10.png?ref_type=heads)

Can see the script is now available and can now execute

![Image 11](https://gitlab.com/FM1995/install-build-tools-in-jenkins-2024/-/raw/main/Images/Image11.png?ref_type=heads)
![Image 12](https://gitlab.com/FM1995/install-build-tools-in-jenkins-2024/-/raw/main/Images/Image12.png?ref_type=heads)

Can now install nodejs and npm

```
apt install nodejs
```

![Image 13](https://gitlab.com/FM1995/install-build-tools-in-jenkins-2024/-/raw/main/Images/Image13.png?ref_type=heads)

Can now see nodejs and npm version

![Image 14](https://gitlab.com/FM1995/install-build-tools-in-jenkins-2024/-/raw/main/Images/Image14.png?ref_type=heads)
![Image 15](https://gitlab.com/FM1995/install-build-tools-in-jenkins-2024/-/raw/main/Images/Image15.png?ref_type=heads)





